﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Picture : MonoBehaviour
{
    public RenderTexture overviewTexture;
    GameObject OVcamera;
    public string path = "";

    void Start()
    {
        OVcamera = GameObject.FindGameObjectWithTag("OverviewCamera");
    }

    void LateUpdate()
    {
        if (Input.GetKeyDown("f9"))
        {
            StartCoroutine(TakeScreenShot());
        }
    }

    // return file name
    string fileName(int width, int height)
    {
        return string.Format("screen_{0}x{1}_{2}.png",
                              width, height,
                              System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    public void TakePictureWithButton()
    {
        StartCoroutine(TakeScreenShot());
        StartCoroutine(Share2());

        //Share();
    }

    public void Share()
    {
        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.LoadRawTextureData(ss.GetRawTextureData());
        ss.Apply();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());

        Destroy(ss);

        new NativeShare().AddFile(filePath).Share();
    }

    public IEnumerator TakeScreenShot()
    {
        yield return new WaitForEndOfFrame();

        Camera camOV = OVcamera.GetComponent<Camera>();
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = camOV.targetTexture;
        camOV.Render();
        Texture2D imageOverview = new Texture2D(camOV.targetTexture.width, camOV.targetTexture.height, TextureFormat.RGB24, false);
        imageOverview.ReadPixels(new Rect(0, 0, camOV.targetTexture.width, camOV.targetTexture.height), 0, 0);
        imageOverview.Apply();
        RenderTexture.active = currentRT;

        // Encode texture into PNG
        byte[] bytes = imageOverview.EncodeToPNG();

        // save in memory
        string filename = fileName(Convert.ToInt32(imageOverview.width), Convert.ToInt32(imageOverview.height));

        if (!Directory.Exists(Application.persistentDataPath + "/Snapshots/" + filename))
        {
            Directory.CreateDirectory(Application.persistentDataPath + "/Snapshots/");
        }

        path = Application.persistentDataPath + "/Snapshots/" + filename;
        print("path: " + path);
        //path = Application.persistentDataPath + path;
        System.IO.File.WriteAllBytes(path, bytes);
    }

    public IEnumerator Share2()
    {
        yield return new WaitForSeconds(0.1f);
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        //ss.LoadRawTextureData(ss.GetRawTextureData());
        ss.Apply();

        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());

        Destroy(ss);

        new NativeShare().AddFile(filePath).Share();
    }
}
